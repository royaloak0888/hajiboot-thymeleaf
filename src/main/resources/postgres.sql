drop sequence customer_seq cascade;
create sequence customer_seq;
drop table customers cascade;
create table customers(id integer default nextval('customer_seq'), first_name text, last_name text);
INSERT INTO customers(first_name, last_name) VALUES('Nobita', 'Nobi');
INSERT INTO customers(first_name, last_name) VALUES('Takeshi', 'Goda');
INSERT INTO customers(first_name, last_name) VALUES('Suneo', 'Honekawa');
INSERT INTO customers(first_name, last_name) VALUES('Shizuka', 'Minamoto');